from flask_restful import Resource, reqparse
from flask_jwt_extended import jwt_required, current_user
from api.models import Poll


class PollView(Resource):
    @jwt_required
    def get(self):
        """Return an list of all poll created. Only registered users"""
        poll = Poll()
        return poll.get_all()

    @jwt_required
    def post(self):
        """
        Returns a poll object created

        :param description: The description of poll.
        """
        parser = reqparse.RequestParser()
        parser.add_argument('description',
                            type=str,
                            required=True,
                            help="This field cannot be blank."
                            )
        args = parser.parse_args()
        poll = Poll(description=args['description'], user_id=current_user.id)
        poll.save()
        return {'message': 'Survey created'}, 201
