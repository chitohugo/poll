from flask_restful import Resource, reqparse
from flask_jwt_extended import create_access_token, jwt_required, current_user
from api.models import User

parser = reqparse.RequestParser()


class Login(Resource):
    def post(self):
        """given the arguments: username and password return a access_token"""
        parser.add_argument('username',
                            type=str,
                            required=True,
                            help="This field cannot be blank."
                            )
        parser.add_argument('password',
                            type=str,
                            required=True,
                            help="This field cannot be blank."
                            )
        args = parser.parse_args()
        user = User.find_by_username(args['username'])

        if user and user.verify_password(args['password']):
            access_token = create_access_token(identity=user.id, fresh=True)
            return {'access_token': access_token}, 200

        return {'message': 'Invalid Credentials'}, 401


class Register(Resource):
    """given the arguments: username, password and email return a access_token and user created"""
    def post(self):
        parser.add_argument('username',
                            type=str,
                            required=True,
                            help='This field cannot be blank.'
                            )
        parser.add_argument('password',
                            type=str,
                            required=True,
                            help='This field cannot be blank.'
                            )
        parser.add_argument('email',
                            type=str,
                            required=True,
                            help='This field cannot be blank.')
        args = parser.parse_args()

        if User.find_by_email(args['email']) or User.find_by_username(args['username']):
            return {'message': 'There is a user with this email or username'}, 400

        user = User(**args)
        user.save()
        access_token = create_access_token(identity=user.id, fresh=True)
        return {'access_token': access_token}, 201
