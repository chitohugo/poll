from flask_restful import Resource, reqparse
from flask_jwt_extended import jwt_required
from api.models import Question, Poll

parser = reqparse.RequestParser()


class QuestionView(Resource):
    # @jwt_required
    def get(self):
        poll = Question()
        return poll

    def post(self):
        """
        Returns a question object created

        :param question: The question.
        :param poll_id: The id to the poll.
        """
        parser.add_argument('question',
                            type=str,
                            required=True,
                            help="This field cannot be blank."
                            )
        parser.add_argument('poll_id',
                            type=str,
                            required=True,
                            help="This field cannot be blank."
                            )
        args = parser.parse_args()
        question = Question(question=args['question'], poll_id=args['poll_id'])
        question.save()
        return {'message': 'Question created'}, 201


class QuestionDetail(Resource):
    @jwt_required
    def get(self, user_id):
        """
        Returns a list of questions by user

        :param user_id: The answer of question.
        """
        questions = Poll.get_id(user_id)
        if questions:
            return {'questions': questions}, 200
        return {'message': 'Not found'}, 404
