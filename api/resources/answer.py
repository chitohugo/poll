from flask_restful import Resource, reqparse
from api.models import Answer
from api.models import Question


class AnswerView(Resource):
    def post(self):
        """
        Returns a answer object created

        :param answer: The answer of question.
        :param question_id: The id to the question.
        """
        parser = reqparse.RequestParser()
        parser.add_argument('answer',
                            type=str,
                            required=True,
                            help="This field cannot be blank."
                            )
        parser.add_argument('question_id',
                            type=str,
                            required=True,
                            help="This field cannot be blank."
                            )
        args = parser.parse_args()
        question = Question.find_by_id(args['question_id'])

        if not question:
            return {'message': 'Not found'}, 404

        if question.limit <= 4:
            answer = Answer(**args)
            answer.save()
            question.limit += 1
            question.save()
            return {'message': 'Answer created'}, 201

        return {'message': 'the allowed answer limit per question has been exceeded'}, 200



