from flask_restful import Api
from api.resources.answer import AnswerView
from .resources.poll import PollView
from .resources.question import QuestionView, QuestionDetail
from .resources.user import Register, Login


def main(app):
    api = Api(app)
    # Endpoints of API
    api.add_resource(Login, '/login')
    api.add_resource(Register, '/register')
    api.add_resource(PollView, '/poll')
    api.add_resource(QuestionView, '/question')
    api.add_resource(QuestionDetail, '/question/<int:user_id>')
    api.add_resource(AnswerView, '/answer')

