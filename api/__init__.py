from flask import Flask
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_jwt_extended import JWTManager

db = SQLAlchemy()
jwt = JWTManager()
migrate = Migrate()

from . import endpoints


def create_app():
    app = Flask(__name__, instance_relative_config=True)
    app.config['SQLALCHEMY_DATABASE_URI'] = 'postgres://postgres:nubi2020@172.21.0.2/poll'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['SQLALCHEMY_ECHO'] = False
    app.config['JWT_SECRET_KEY'] = 'NUBI.2@2@2!'
    app.config['JWT_ACCESS_TOKEN_EXPIRES'] = False
    db.init_app(app)
    migrate.init_app(app, db)
    jwt.init_app(app)

    from .models import User

    @jwt.user_loader_callback_loader
    def user_loader_callback(identity):
        user = User.query.get(identity)
        if not user:
            return None
        return user

    endpoints.main(app)
    return app
