from . import db
import hashlib
import uuid
import time


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password_hash = db.Column(db.String(128), nullable=True)
    pwd_salt = db.Column(db.String(32), nullable=True)
    polls = db.relationship('Poll', backref='user', lazy=True)

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @property
    def password(self):
        """
        Prevents password from being accessed
        """
        raise AttributeError("Password is not a readable attribute.")

    @password.setter
    def password(self, password):
        """
        Sets a password to a hashed password
        """
        salt = uuid.uuid4().hex
        self.pwd_salt = salt
        self.password_hash = hashlib.sha512(password.encode('utf-8') + salt.encode('utf-8')).hexdigest()

    def verify_password(self, password):
        """
        Checks if hashed password matches actual password
        """
        if self.password_hash == hashlib.sha512(password.encode('utf-8') + self.pwd_salt.encode('utf-8')).hexdigest():
            return True

    @classmethod
    def find_by_email(cls, email):
        return cls.query.filter_by(email=email).first()

    @classmethod
    def find_by_username(cls, username):
        return cls.query.filter_by(username=username).first()

    def __repr__(self):
        return f'{self.email}'


class Poll(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(255))
    created_date = db.Column(db.DateTime, default=time.strftime("%Y-%m-%d %H:%M:%S"))
    user_id = db.Column(db.ForeignKey('user.id',
                                      ondelete=u'CASCADE',
                                      onupdate=u'CASCADE'),
                        nullable=False)
    questions = db.relationship('Question', backref='poll', lazy=True)

    def __repr__(self):
        return f'{self.description}'

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self

    @classmethod
    def get_all(cls):
        response = []
        polls = cls.query.all()
        for item in polls:
            poll = {
                'id': item.id,
                'description': item.description,
                'created_date': str(item.created_date),
                'user': str(item.user)
            }
            if item.questions:
                question = []
                for value in item.questions:
                    question.append({
                        'id': value.id,
                        'question': value.question,
                        'answer': [{'id': value.id, 'answer': value.answer} for value in value.answers]
                    })
                poll['questions'] = question
            response.append(poll)
        return response

    @classmethod
    def get_id(csl, id):
        polls = csl.query.filter_by(user_id=id).all()
        if polls:
            response = []
            for poll in polls:
                if poll.questions:
                    for question in poll.questions:
                        qst = {
                            'id': question.id,
                            'question': question.question,
                            'created date': str(question.created_date)
                        }
                        response.append(qst)
            return response
        return None


class Question(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    question = db.Column(db.String(255))
    created_date = db.Column(db.DateTime, default=time.strftime("%Y-%m-%d %H:%M:%S"))
    poll_id = db.Column(db.ForeignKey('poll.id',
                                      ondelete=u'CASCADE',
                                      onupdate=u'CASCADE'),
                        nullable=False)
    limit = db.Column(db.Integer, default=1)
    answers = db.relationship('Answer', backref='question', lazy=True)

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self

    def __repr__(self):
        return f'{self.question}'

    @classmethod
    def find_by_id(cls, id):
        return cls.query.filter_by(id=id).first()


class Answer(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    answer = db.Column(db.String(255))
    created_date = db.Column(db.DateTime, default=time.strftime("%Y-%m-%d %H:%M:%S"))
    question_id = db.Column(db.ForeignKey('question.id',
                                          ondelete=u'CASCADE',
                                          onupdate=u'CASCADE'),
                            nullable=False)

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self

    def __repr__(self):
        return f'{self.answer}'
