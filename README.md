# API for Poll

## Running with docker

### Pre-requisites:
- docker
- docker-compose

### Steps
2. Build with `docker-compose build`
2. Run with `docker-compose up`

### Como funciona:
1. Debe registrarse: Endpoint: /register o /login, si ya esta registrado.
2. Con el access_token otorgado en el paso previo, puede crear una encuesta. Endpoint: /poll 
3. Crear preguntas a la encuesta creada previamente. Endpoint: /question
4. Crear respuestas a las preguntas creadas. Endpoint: /answer

### Desafio

1. Tenia tiempo sin tocar el framework d Flask, se me habia olvidalo muchas cosas. "Leer Doc...".
2. No encontraba por donde encarar la prueba, decidi armar la estructura del projecto, tener esto listo sin ningun problema era fundamental, ya luego me puse a leer nuevamente los requerimientos identique las entidades que iba a necesitar, arme las clases del modelo, comence por el registro de usuario, login y termine creando los endpoints para cada modelo y creando los metodos necesarios.

